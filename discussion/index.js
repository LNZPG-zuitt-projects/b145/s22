// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// {
// 	"number": 1
// 	"boolean": true
// }

// JS Array of Object
let myArr = [
	{name: "Jino"},
	{name: "John"}
]

// Array of JSON Object
// {
// 	"cities": [
// 		{"city": "Quezon City"}
// 		{"city": "Makati City"}
// 	]
// }


// converting JS Data into Stringified JSON
// -Stringified JSON is a JS Object converted into a string to be used by the recceiveing application or other functions of a JS application

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'}
]

'[{"batchname":"Batch 145"},{"batchname":"Batch 146"},{"batchname":"Batch 147"}]'

let stringifiedData = JSON.stringify(batchesArr)
// console.log(stringifiedData);

// Converting Stringified JSON into JS Objects

let fixedData = JSON.parse(stringifiedData)
console.log(fixedData)